#include <iostream>
#include <vector>
#include <string>
#include <limits>

#include "Game.h"


template<class T>
void printContainer(const T& container)
{
    std::cout << "\n";
    for (const auto& currentElement : container)
    {
        std::cout << currentElement << " ";
    }
    std::cout << "\n";
}

bool checkTestCase(const std::string& testName, const std::vector<unsigned int>& input, const std::vector<unsigned int>& expected, const bool printResults)
{
    if (printResults)
    {
        std::cout << "\n\nTesting: " << testName << "\n";
        std::cout << "Hits: ";
        printContainer(input);
        std::cout << "Expected scores: ";
        printContainer(expected);
    }
    
    const std::vector<unsigned int> output = Game::calculateScores(input, printResults);
    if (expected != output)
    {
        std::cout << "\n" << testName << " failed!\n";
        std::cout << "Output:";
        printContainer(output);
        std::cout << "Expecting:";
        printContainer(expected);
        return false;
    }
    if (printResults) std::cout << "Test passed.\n";
    return true;
}

bool run_tests(const bool printResults)
{
    const std::vector<unsigned int> test_case_1{ 1, 4, 4, 5, 6, 4, 5, 5, 10, 0, 1, 7, 3, 6, 4, 10, 2, 8, 6 };
    const std::vector<unsigned int> test_scores_1{ 5, 14, 29, 49, 60, 61, 77, 97, 117, 133 };
    if (!checkTestCase("Test_case_1", test_case_1, test_scores_1, printResults))
    {
        return false;
    }

    const std::vector<unsigned int> test_case_2{ 9, 0, 3, 5, 6, 1, 3, 6, 8, 1, 5, 3, 2, 5, 8, 0, 7, 1, 8, 1 };
    const std::vector<unsigned int> test_scores_2{ 9, 17, 24, 33, 42, 50, 57, 65, 73, 82 };
    if (!checkTestCase("Test_case_2", test_case_2, test_scores_2, printResults))
    {
        return false;
    }

    const std::vector<unsigned int> test_case_3{ 9, 0, 3, 7, 6, 1, 3, 7, 8, 1, 5, 5, 0, 10, 8, 0, 7, 3, 8, 2, 8 };
    const std::vector<unsigned int> test_scores_3{ 9, 25, 32, 50, 59, 69, 87, 95, 113, 131 };
    if (!checkTestCase("Test_case_3", test_case_3, test_scores_3, printResults))
    {
        return false;
    }

    const std::vector<unsigned int> test_case_4{ 10, 3, 7, 6, 1, 10, 10, 10, 2, 8, 9, 0, 7, 3, 10, 10, 10 };
    const std::vector<unsigned int> test_scores_4{ 20, 36, 43, 73, 95, 115, 134, 143, 163, 193 };
    if (!checkTestCase("Test_case_4", test_case_4, test_scores_4, printResults))
    {
        return false;
    }

    const std::vector<unsigned int> test_case_5{ 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 };
    const std::vector<unsigned int> test_scores_5{ 30, 60, 90, 120, 150, 180, 210, 240, 270, 300 };
    if (!checkTestCase("Test_case_5", test_case_5, test_scores_5, printResults))
    {
        return false;
    }

    return true;
}


int main(int argc, char** argv)
{
    const bool use_tests{ true };
    const bool print_results_of_tests{ false };
    
    if (use_tests)
    {
        std::cout << "Running tests...";
        if (!run_tests(print_results_of_tests))
        {
            std::cout << "\nOne of the tests failed, the game could be incorrect, exiting...";
            return -1;
        }
        else
        {
            std::cout << " All tests have been passed.\n\n";
        }
    }

    // main game loop
    while (true)
    {
        int playersCount{ 0 };
        std::cout << "How many players do you have: ";
        while (playersCount <= 0)
        {
            std::cin >> playersCount;
            if (playersCount <= 0)
            {
                std::cout << "Please enter a positive integer.\n";
            }
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }

        std::vector<std::string> names;
        names.reserve(playersCount);
        for (int currentPlayer = 0; currentPlayer < playersCount; ++currentPlayer)
        {
            std::cout << "Please enter the name for the player " << currentPlayer + 1 << ": ";
            std::string currentPlayerName;
            std::cin >> currentPlayerName;
            names.push_back(currentPlayerName);
        }
    
        Game game(names);
        game.startNewGame();

        char c(0);
        while (c != 'y' && c != 'n' && c != 'Y' && c != 'N')
        {
            std::cout << "Do you want to continue playing (y/n)?: ";
            std::cin >> c;
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
        if (c == 'n' || c == 'N')
        {
            std::cout << "Exiting the game...\n";
            break;
        }
    }

    return 0;
}
