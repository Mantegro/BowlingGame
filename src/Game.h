#pragma once

#include "Frame.h"
#include "Player.h"

#include <vector>
#include <string>

class Game
{
public:
    static const size_t MaxNumberOfFrames{ 10 };

    Game(const std::vector<std::string>& players);
    void startNewGame();
    
    static const std::vector<unsigned int> calculateScores(const std::vector<unsigned int>& pinsPerThrow, const bool printResults = false);
    static void printScores(const std::vector<Player>& players);

private:
    std::vector<Player> m_vPlayers;
    size_t m_currentPlayer;
    size_t m_currentFrame;
};
