#include "Frame.h"
#include <iostream>


Frame::Frame()
    : m_uiFirstThrowPins(0)
    , m_bFirstThrowUsed(false)
    , m_uiSecondThrowPins(0)
    , m_bSecondThrowUsed(false)
    , m_uiThirdThrowPins(0)
    , m_bThirdThrowUsed(false)
    , m_uiBonusScores(0)
    , m_bIsSpare(false)
    , m_bIsStrike(false)
    , m_bIsSecondStrike(false)
    , m_bIsFinished(false)
{

}


bool Frame::play(const bool isLastFrame, const bool useUserInput, const unsigned int pinsForThrow)
{
    if (!m_bFirstThrowUsed)
    {
        if (useUserInput) std::cout << "First throw.\n";
        m_uiFirstThrowPins = useUserInput ? inputPinHitsForThrow(s_maxPins) : pinsForThrow;
        m_bFirstThrowUsed = true;
        if (m_uiFirstThrowPins == s_maxPins)
        {
            m_bIsStrike = true;
            if (useUserInput) std::cout << "You've got a STRIKE!\n";
            return isLastFrame;
        }
        return true;
    }
    
    if (!m_bSecondThrowUsed)
    {
        if (useUserInput) std::cout << "Second throw.\n";
        m_uiSecondThrowPins = useUserInput ? ((isLastFrame && m_bIsStrike) ? inputPinHitsForThrow(s_maxPins) : inputPinHitsForThrow(s_maxPins - m_uiFirstThrowPins)) : pinsForThrow;
        m_bSecondThrowUsed = true;
        if (isLastFrame && m_bIsStrike && m_uiSecondThrowPins == s_maxPins)
        {
            m_bIsSecondStrike = true;
            if (useUserInput) std::cout << "You've got a STRIKE!\n";
            return true;
        }

        if (m_uiFirstThrowPins + m_uiSecondThrowPins == s_maxPins)
        {
            m_bIsSpare = true;
            if (useUserInput) std::cout << "You've got a SPARE!\n";
            return isLastFrame;
        }
        m_bIsFinished = true;
        return false;
    }
    
    if (isLastFrame && (m_bIsStrike || m_bIsSpare))
    {
        if (useUserInput) std::cout << "Third throw.\n";
        m_bThirdThrowUsed = true;
        m_uiThirdThrowPins = useUserInput ? ((m_bIsSpare || m_bIsSecondStrike) ? inputPinHitsForThrow(s_maxPins) : inputPinHitsForThrow(s_maxPins - m_uiSecondThrowPins)) : pinsForThrow;
        m_uiBonusScores = m_uiThirdThrowPins;
        if (m_uiThirdThrowPins == s_maxPins)
        {
            if (useUserInput) std::cout << "You've got a STRIKE!\n";
        }
    }
    return false;
}



unsigned int Frame::inputPinHitsForThrow(const int pinsRemaining)
{
    int scores{ -1 };
    bool validValue = false;
    while (!validValue)
    {
        std::cout << "Please enter a positive integer in a range 0 - " << pinsRemaining << ": ";
        validValue = (std::cin >> scores) && (scores >= 0) && (scores <= pinsRemaining);
        std::cin.clear();
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }
    return scores;
}

