#pragma once

#include <string>
#include <vector>
#include "Frame.h"

class Player
{
public:
    Player(const std::string& name, bool useUserInput = true, const std::vector<unsigned int>& pinsPerThrow = {});

    // returns true if the frame was not completed yet and the player still have to do a throw
    bool doThrow(const size_t frameIndex);

    const Frame& getFrame(const size_t frameIndex) const;
    const std::vector<unsigned int> getOverallScores() const;
    const std::string& getName() const { return m_name; }

private:
    void finishPreviousFrames(const size_t frameIndex);

    std::string m_name;
    std::vector<Frame> m_vFrames;
    bool m_bUseUserInput;
    std::vector<unsigned int> m_vPinsPerThrow;
    size_t m_currentInputIndex;
};
