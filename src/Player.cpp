#include "Player.h"
#include "Game.h"

#include <iostream>
#include <assert.h>

Player::Player(const std::string& name, bool useUserInput, const std::vector<unsigned int>& pinsPerThrow)
    : m_name(name)
    , m_bUseUserInput(useUserInput)
    , m_vPinsPerThrow(pinsPerThrow)
    , m_currentInputIndex(0)
{
    m_vFrames.resize(Game::MaxNumberOfFrames);
}

const Frame& Player::getFrame(const size_t frameIndex) const
{
    assert(frameIndex < m_vFrames.size());
    return m_vFrames[frameIndex];
}


void Player::finishPreviousFrames(const size_t frameIndex)
{
    if (frameIndex == 0)
    {
        return;
    }

    Frame& previousFrame = m_vFrames[frameIndex - 1];
    if (previousFrame.isFinished())
    {
        return;
    }

    bool finishFrames{ true };
    const Frame& currentFrame = m_vFrames[frameIndex];
    if (previousFrame.isSpare() && currentFrame.getFirstThrowUsed())
    {
        previousFrame.setBonusScores(currentFrame.getFirstThrowPins());
    }
    else if (previousFrame.isStrike())
    {
        if (currentFrame.getFirstThrowUsed() && currentFrame.getSecondThrowUsed())
        {
            previousFrame.setBonusScores(currentFrame.getFirstThrowPins() + currentFrame.getSecondThrowPins());
        }
        else if (currentFrame.isStrike() && currentFrame.isFinished())
        {
            if (frameIndex < Game::MaxNumberOfFrames - 1)
            {
                const Frame& nextFrame = m_vFrames[frameIndex + 1];
                previousFrame.setBonusScores(currentFrame.getFirstThrowPins() + nextFrame.getFirstThrowPins());
            }
            else
            {
                previousFrame.setBonusScores(currentFrame.getFirstThrowPins() + currentFrame.getSecondThrowPins());
            }
        }
        else
        {
            finishFrames = false;
        }
    }
    else
    {
        finishFrames = false;
    }

    if (finishFrames)
    {
        previousFrame.finishFrame();
        finishPreviousFrames(frameIndex - 1);
    }
}


bool Player::doThrow(const size_t frameIndex)
{
    assert(frameIndex < Game::MaxNumberOfFrames);

    Frame& currentFrame = m_vFrames[frameIndex];
    const bool isLastFrame{ frameIndex == Game::MaxNumberOfFrames - 1 };

    while (currentFrame.play(isLastFrame, m_bUseUserInput, m_bUseUserInput ? 0 : m_vPinsPerThrow[m_currentInputIndex++]))
    {
        finishPreviousFrames(frameIndex);
        if (!m_bUseUserInput && (m_currentInputIndex >= m_vPinsPerThrow.size()))
        {
            std::cout << "Not enough throws!";
            return false;
        }
        return true;
    }
    if (isLastFrame)
    {
        currentFrame.finishFrame();
    }
    finishPreviousFrames(frameIndex);
    return false;
}

const std::vector<unsigned int> Player::getOverallScores() const
{
    size_t currentFrameIndex{ 0 };
    std::vector<unsigned int> scores(m_vFrames.size(), 0);
    for (const auto& currentFrame : m_vFrames)
    {
        if (currentFrameIndex > 0)
        {
            scores[currentFrameIndex] += scores[currentFrameIndex - 1];
        }
        scores[currentFrameIndex++] += currentFrame.getFirstThrowPins() + currentFrame.getSecondThrowPins() + currentFrame.getBonusScores();
    }
    return scores;
}
