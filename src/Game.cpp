#include "Game.h"

#include <iostream>
#include <iomanip>

Game::Game(const std::vector<std::string>& players)
    : m_currentPlayer(0)
    , m_currentFrame(0)
{
    m_vPlayers.reserve(players.size());
    for (const auto& currentPlayersName : players)
    {
        m_vPlayers.emplace_back(currentPlayersName);
    }
}


void Game::printScores(const std::vector<Player>& players)
{
    size_t maxPlayersNameLength{ 0 };
    for (const auto& currentPlayer : players)
    {
        if (maxPlayersNameLength < currentPlayer.getName().size())
        {
            maxPlayersNameLength = currentPlayer.getName().size();
        }
    }

    std::cout << std::endl;
    
    const auto drawDelimiter = [&] {
        std::cout << std::setw(1) << "|";
        std::cout.fill('-');
        std::cout << std::right << std::setw(1 + maxPlayersNameLength + 1 + 6 * MaxNumberOfFrames + 2) << "|\n";
    };

    drawDelimiter();

    // draw frames counter
    std::cout.fill(' ');
    std::cout << std::setw(1) << "|" << std::right << std::setw(maxPlayersNameLength + 1) << "|";
    for (size_t currentFrameIndex = 0; currentFrameIndex < MaxNumberOfFrames - 1; ++currentFrameIndex)
    {
        std::cout << std::setw(1) << "  " << std::left << std::setw(3) << currentFrameIndex + 1 << std::setw(1) << "|";
    }
    std::cout << "   " << std::left << std::setw(4) << MaxNumberOfFrames << std::setw(1) << "|\n";

    drawDelimiter();

    for (const auto& currentPlayer : players)
    {
        //draw players name
        std::cout.fill(' ');
        std::cout << std::setw(1) << "|" << std::left << std::setw(maxPlayersNameLength) << currentPlayer.getName() << std::setw(1) << "|";

        // draw scores for each frame
        for (size_t currentFrameIndex = 0; currentFrameIndex < MaxNumberOfFrames - 1; ++currentFrameIndex)
        {
            const Frame& currentFrame = currentPlayer.getFrame(currentFrameIndex);
            if (!currentFrame.getFirstThrowUsed())
            {
                std::cout << "     |";
            }
            else if (currentFrame.isStrike())
            {
                std::cout << "  X  |";
            }
            else
            {
                std::cout << " " << currentFrame.getFirstThrowPins() << " ";
                if (!currentFrame.getSecondThrowUsed())
                {
                    std::cout << "  |";
                }
                else if (currentFrame.isSpare())
                {
                    std::cout << "/ |";
                }
                else
                {
                    std::cout << currentFrame.getSecondThrowPins() << " |";
                }
            }
        }
        const Frame& lastFrame = currentPlayer.getFrame(MaxNumberOfFrames - 1);
        if (!lastFrame.getFirstThrowUsed())
        {
            std::cout << "       |\n";
        }
        else
        {
            if (lastFrame.isStrike())
            {
                std::cout << " X";
            }
            else
            {
                std::cout << " " << lastFrame.getFirstThrowPins();
            }

            if (lastFrame.getSecondThrowUsed())
            {
                if (lastFrame.isSpare())
                {
                    std::cout << " /";
                }
                else if (lastFrame.isSecondStrike())
                {
                    std::cout << " X";
                }
                else
                {
                    std::cout << " " << lastFrame.getSecondThrowPins();
                }

                if (lastFrame.isFinished() && (lastFrame.isSpare() || lastFrame.isStrike()))
                {
                    if (lastFrame.getThirdThrowPins() == Frame::s_maxPins)
                    {
                        std::cout << " X |\n";
                    }
                    else
                    {
                        std::cout << " " << lastFrame.getThirdThrowPins() << " |\n";
                    }
                }
                else
                {
                    std::cout << "   |\n";
                }
            }
            else
            {
                std::cout << "     |\n";
            }
        }

        // draw overall scores
        std::cout.fill(' ');
        std::cout << std::setw(1) << "|" << std::right << std::setw(maxPlayersNameLength + 1) << "|";
        unsigned int scoresCounter = 0;
        for (size_t currentFrameIndex = 0; currentFrameIndex < MaxNumberOfFrames; ++currentFrameIndex)
        {
            const Frame& currentFrame = currentPlayer.getFrame(currentFrameIndex);
            if (currentFrame.isFinished())
            {
                scoresCounter += currentFrame.getFirstThrowPins() + currentFrame.getSecondThrowPins() + currentFrame.getBonusScores();
                if (currentFrameIndex == MaxNumberOfFrames - 1) std::cout << " ";
                std::cout << std::setw(1) << " " << std::left << std::setw(3) << scoresCounter << std::setw(1);
                if (currentFrameIndex == MaxNumberOfFrames - 1) std::cout << " ";
                std::cout << " |";
            }
            else
            {
                if (currentFrameIndex == MaxNumberOfFrames - 1) std::cout << "  ";
                std::cout << std::setw(1) << "     |";
            }
            if (currentFrameIndex == MaxNumberOfFrames - 1) std::cout << "\n";
        }

        drawDelimiter();
    }
    std::cout << std::endl;
}


void Game::startNewGame()
{
    m_currentPlayer = 0;
    m_currentFrame = 0;
    while (m_currentFrame < MaxNumberOfFrames)
    {
        for (auto& currentPlayer : m_vPlayers)
        {
            printScores(m_vPlayers);
            std::cout << "Current player is: " << currentPlayer.getName() << ", frame: " << m_currentFrame + 1 << std::endl;
            // do all possible throws for the current player
            while(currentPlayer.doThrow(m_currentFrame))
            {
                printScores(m_vPlayers);
            }
        }
        ++m_currentFrame;
    }
    printScores(m_vPlayers);
}


const std::vector<unsigned int> Game::calculateScores(const std::vector<unsigned int>& pinsPerThrow, const bool printResults)
{
    size_t currentFrame = 0;
    Player player("TestPlayer", false, pinsPerThrow);
    while (currentFrame < MaxNumberOfFrames)
    {
        while (player.doThrow(currentFrame))
        {
            
        }
        ++currentFrame;
    }
    
    if (printResults)
    {
        std::vector<Player> players;
        players.push_back(player);
        printScores(players);
    }
    
    return player.getOverallScores();
}
