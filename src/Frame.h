#pragma once

class Frame
{
public:
    static const unsigned int s_maxPins{ 10 };

    Frame();
    bool play(const bool isLastFrame, const bool useUserInput = true, const unsigned int pinsForThrow = 0);

    bool isStrike() const { return m_bIsStrike; }
    bool isSecondStrike() const { return m_bIsSecondStrike;  }
    bool isSpare() const { return m_bIsSpare; }
    bool getFirstThrowUsed() const { return m_bFirstThrowUsed; }
    bool getSecondThrowUsed() const { return m_bSecondThrowUsed; }
    
    unsigned int getFirstThrowPins() const { return m_uiFirstThrowPins; }
    unsigned int getSecondThrowPins() const { return m_uiSecondThrowPins; }
    unsigned int getThirdThrowPins() const { return m_uiThirdThrowPins; }
    unsigned int getBonusScores() const { return m_uiBonusScores; }
    void setBonusScores(const unsigned int bonusScores) { m_uiBonusScores = bonusScores; }
    bool isFinished() const { return m_bIsFinished; }
    void finishFrame() { m_bIsFinished = true; }

private:
    static unsigned int inputPinHitsForThrow(const int maxScores);

    unsigned int m_uiFirstThrowPins;
    bool m_bFirstThrowUsed;

    unsigned int m_uiSecondThrowPins;
    bool m_bSecondThrowUsed;

    unsigned int m_uiThirdThrowPins;
    bool m_bThirdThrowUsed;

    unsigned int m_uiBonusScores;
    
    bool m_bIsSpare;
    bool m_bIsStrike;
    bool m_bIsSecondStrike;
    bool m_bIsFinished;
};
